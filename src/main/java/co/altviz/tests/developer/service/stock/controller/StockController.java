package co.altviz.tests.developer.service.stock.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/stocks")
public class StockController {

  @GetMapping
  public ResponseEntity<Object> getClosingStocks() {
    return null;
  }
}
