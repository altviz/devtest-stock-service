# Stock Service

## Environment Requirements
* Java: 11
* Maven: 3

## Starting application

Do whatever you are comfortable with

Option A:
Run from within IDE

Option B:
Run from command line:
```
mvn spring-boot:run
```